//------------------------------------------------------------------------------
// Project CharacterLib
//
// Copyright (c) 2017 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                  License                                   
//                                                                            
//  This program is free software: you can redistribute it and/or modify      
//  it under the terms of the GNU Lesser General Public License as published  
//  by the Free Software Foundation, either version 3 of the License, or      
//  (at your option) any later version.                                       
//                                                                            
//  This program is distributed in the hope that it will be useful,           
//  but WITHOUT ANY WARRANTY; without even the implied warranty of            
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             
//  GNU Lesser General Public License for more details.                       
//                                                                            
//  You should have received a copy of the GNU Lesser General Public License  
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.     
//------------------------------------------------------------------------------

#ifndef PROJECT_CHARACTERLIB_SUPPRESS_WARNINGS_
#define PROJECT_CHARACTERLIB_SUPPRESS_WARNINGS_

#if defined __clang__
#define SUPPRESS_WARNINGS_BEGIN                     \
  _Pragma("clang diagnostic push")                  \
      _Pragma("clang diagnostic ignored \"-Wall\"") \
          _Pragma("clang diagnostic ignored \"-Wextra\"")
#define SUPPRESS_WARNINGS_END _Pragma("clang diagnostic pop")

#elif defined _MSC_VER
#define NOMINMAX
#define SUPPRESS_WARNINGS_BEGIN __pragma(warning(push, 0));
#define SUPPRESS_WARNINGS_END __pragma(warning(pop));

#elif defined __GNUC__
#define SUPPRESS_WARNINGS_BEGIN                                                \
  _Pragma("GCC diagnostic push") _Pragma("GCC diagnostic ignored \"-Wall\"")   \
      _Pragma("GCC diagnostic ignored \"-Wextra\"")                            \
          _Pragma("GCC diagnostic ignored \"-Wmissing-braces\"")               \
              _Pragma("GCC diagnostic ignored \"-Wpedantic\"")                 \
			  _Pragma("GCC diagnostic ignored \"-Wunused-but-set-variable\"")  \
			  _Pragma("GCC diagnostic ignored \"-Wunused-local-typedefs\"")    \
                  _Pragma("GCC diagnostic ignored \"-Wunused-parameter\"") 
                      _Pragma("GCC diagnostic ignored \"-Wdeprecated-declarations\"")

#define SUPPRESS_WARNINGS_END _Pragma("GCC diagnostic pop")

#endif

#endif  // PROJECT_CHARACTERLIB_SUPPRESS_WARNINGS_
