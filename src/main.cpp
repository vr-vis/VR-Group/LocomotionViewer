/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include "LocomotionViewer.h"

#include <memory>

int main(int argc, char *argv[]) {
  try {
    std::unique_ptr<LocomotionViewer> locomotionViewer =
        std::make_unique<LocomotionViewer>();
    if (locomotionViewer->Init(argc, argv) == true) {
      // start universe as an endless loop
      locomotionViewer->Run();
    }
  } catch (VistaExceptionBase &e) {
    e.PrintException();
  } catch (std::exception &e) {
    std::cerr << "Exception:" << e.what() << std::endl;
  }

  return 0;
}
