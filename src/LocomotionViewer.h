/*============================================================================*/
/* This class will read in scenes from xml files and visualize them on a      */
/* SceneGraph.	Simply initialize a SceneReader and use the Read method.      */
/*============================================================================*/

#ifndef SRC_LOCOMOTIONVIEWER_H_
#define SRC_LOCOMOTIONVIEWER_H_

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

#include <memory>
#include <vector>

#include <CharacterLib/Entity.hpp>

SUPPRESS_WARNINGS_BEGIN
#include <VistaKernel/VistaSystem.h>

// Vista Events
#include <VistaKernel/EventManager/VistaCentralEventHandler.h>
#include <VistaKernel/EventManager/VistaEventManager.h>
#include <VistaKernel/EventManager/VistaSystemEvent.h>
SUPPRESS_WARNINGS_END

#include "CallbackContainer.h"
#include "ConfigurationParser.h"

#include <CharacterLib/Components/LocomotionCmp.hpp>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class VistaEventManager;
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class LocomotionViewer {
public:
  LocomotionViewer() = default;
  virtual ~LocomotionViewer();

  bool Init(int argc, char *argv[]);
  void Run();

private:
  void ReadConfigurationFile();
  void CreateCharacters();
  void CreateCallbacks();

  CharacterLib::WalkingCycleData walkingCycleData_;

  std::shared_ptr<CallbackContainer> callbackContainer_;

  std::unique_ptr<ConfigurationParser> configurationParser_;
  std::unique_ptr<VistaSystem> vistaSystem_;
  std::vector<CharacterLib::Entity *> characters_;
  std::vector<std::string> animationNames_;
  VistaEventManager *eventManager_ = nullptr;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

#endif // SRC_LOCOMOTIONVIEWER_H_
