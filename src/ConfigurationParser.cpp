/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "ConfigurationParser.h"

SUPPRESS_WARNINGS_BEGIN

#include <string>

#include "VistaBase/VistaStreamUtils.h"
#include "VistaKernel/InteractionManager/VistaKeyboardSystemControl.h"
#include "VistaKernel/VistaSystem.h"
#include "VistaTools/VistaFileSystemDirectory.h"
#include "VistaTools/VistaFileSystemFile.h"
#include "VistaTools/VistaFileSystemNode.h"
#include "VistaTools/VistaStreams.h"

#include "sb/SBScene.h"

#include <VistaTools/VistaIniFileParser.h>

SUPPRESS_WARNINGS_END

#include <CharacterLib/Components/LocomotionCmp.hpp>
#include <CharacterLib/Helpers/Logger.hpp>

std::vector<std::string> ConfigurationParser::ReadConfigurationFile(
    std::string fileName, CharacterLib::WalkingCycleData &walkingCycleData) {
  VistaIniFileParser oFile;

  std::list<std::string> liCfgPaths;
  liCfgPaths.clear();
  liCfgPaths.push_back("");
  if (oFile.ReadFile(fileName) == false) {
    vstr::err() << "[ConfigurationParser::ConfigurationParser]: Couldn't parse "
                << fileName << "." << std::endl;
  }
  ParseConfiguration(oFile.GetPropertyList(), walkingCycleData);
  return ParseAnimationNames(oFile.GetPropertyList());
}

void ConfigurationParser::ParseConfiguration(
    VistaPropertyList propertyList,
    CharacterLib::WalkingCycleData &walkingCycleData) {

  walkingCycleData.animationFile_ =
      propertyList.GetValue<std::string>("ANIMATION_NAME");
  walkingCycleData.scale_ = propertyList.GetValue<double>("SCALE");
  walkingCycleData.walkSpeedGain_ = propertyList.GetValue<double>("SPEED");
}

std::vector<std::string>
ConfigurationParser::ParseAnimationNames(VistaPropertyList propertyList) {
  auto fullMocapDataPath = SmartBody::SBScene::getScene()->getMediaPath() +
                           "/" +
                           propertyList.GetValue<std::string>("ANIMATION_PATH");
  VistaFileSystemDirectory sysDir(fullMocapDataPath);
  if (!sysDir.Exists()) {
    CharacterLib::LogError()
        << "[CharacterAnimationApp::ReadAnimationNames] folder doesn't exist!"
        << std::endl;
  }

  std::vector<std::string> animationNames;
  animationNames.clear();
  for (auto fileSystemNode : sysDir) {
    auto motionName = fileSystemNode->GetLocalName();
    animationNames.push_back(motionName);
  }
  return animationNames;
}
