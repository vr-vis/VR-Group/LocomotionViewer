/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "CallbackContainer.h"

SUPPRESS_WARNINGS_BEGIN
#include "VistaBase/VistaStreamUtils.h"
#include "VistaKernel/InteractionManager/VistaKeyboardSystemControl.h"
#include "VistaKernel/VistaSystem.h"
#include "VistaTools/VistaFileSystemDirectory.h"
#include "VistaTools/VistaFileSystemFile.h"
#include "VistaTools/VistaFileSystemNode.h"
#include "VistaTools/VistaStreams.h"

#include "sb/SBScene.h"
SUPPRESS_WARNINGS_END

#include <CharacterLib/Components/LocomotionCmp.hpp>
#include <CharacterLib/Components/RenderCmp.hpp>
#include <CharacterLib/Components/TransformationCmp.hpp>
#include <CharacterLib/Entity.hpp>

CallbackContainer::CallbackContainer(
    std::vector<CharacterLib::Entity *> characters,
    std ::vector<std::string> animationNames,
    CharacterLib::WalkingCycleData walkingCycleData) {
  animtionNames_ = animationNames;
  walkingCycleData_ = walkingCycleData;
  characters_ = characters;
  SaveStartPositions();
  CreateWalkPath();
}

void CallbackContainer::StartWalking() {
  unsigned int characterIndex = 0;
  for (auto character : characters_) {
    auto locomotionCmp = character->GetComponent<CharacterLib::LocomotionCmp>();

    std::vector<VistaVector3D> translatedWalkPath;
    translatedWalkPath.clear();
    for (auto wayPoint : wayPath_) {
      translatedWalkPath.push_back(wayPoint + startPositions_[characterIndex]);
    }

    auto transformationCmp =
        character->GetComponent<CharacterLib::TransformationCmp>();
    transformationCmp->SetPosition(startPositions_[characterIndex]);
    transformationCmp->SetOrientation(VistaQuaternion(
        translatedWalkPath[0], startPositions_[characterIndex]));

    locomotionCmp->SetTargetPositions(translatedWalkPath);
    characterIndex++;
  }
}

void CallbackContainer::StopWalking() {
  for (auto character : characters_) {
    auto locomotionCmp = character->GetComponent<CharacterLib::LocomotionCmp>();
    locomotionCmp->SetTargetPosition(
        locomotionCmp->ApproximateStopLocation(0.1f));
  }
}

void CallbackContainer::SwitchMode() {
  if (inSingleAnimationMode == true) {
    inSingleAnimationMode = false;
    for (auto character : characters_) {
      auto renderCmp = character->GetComponent<CharacterLib::RenderCmp>();
      auto locomotionCmp =
          character->GetComponent<CharacterLib::LocomotionCmp>();
      if (renderCmp != nullptr && locomotionCmp != nullptr) {
        renderCmp->SetTextOverlayText(animtionNames_[currentAnimation_]);
        CharacterLib::WalkingCycleData walkingCycleDataCopy = walkingCycleData_;
        walkingCycleDataCopy.animationFile_ = animtionNames_[currentAnimation_];
        locomotionCmp->SetWalkingCycle(walkingCycleDataCopy);
      }
    }
  } else {
    inSingleAnimationMode = true;
    for (auto character : characters_) {
      auto renderCmp = character->GetComponent<CharacterLib::RenderCmp>();
      auto locomotionCmp =
          character->GetComponent<CharacterLib::LocomotionCmp>();
      if (renderCmp != nullptr && locomotionCmp != nullptr) {
        renderCmp->SetTextOverlayText(walkingCycleData_.animationFile_);
        locomotionCmp->SetWalkingCycle(walkingCycleData_);
      }
    }
  }
}

void CallbackContainer::NextAnimation() {
  if (inSingleAnimationMode == false) {
    currentAnimation_++;
    if (currentAnimation_ == animtionNames_.size()) {
      currentAnimation_ = 0;
    }

    for (auto character : characters_) {
      auto renderCmp = character->GetComponent<CharacterLib::RenderCmp>();
      auto locomotionCmp =
          character->GetComponent<CharacterLib::LocomotionCmp>();
      if (renderCmp != nullptr && locomotionCmp != nullptr) {
        renderCmp->SetTextOverlayText(animtionNames_[currentAnimation_]);
        CharacterLib::WalkingCycleData walkingCycleDataCopy = walkingCycleData_;
        walkingCycleDataCopy.animationFile_ = animtionNames_[currentAnimation_];
        locomotionCmp->SetWalkingCycle(walkingCycleDataCopy);
      }
    }
  }
}

void CallbackContainer::SaveStartPositions() {
  startPositions_.clear();
  for (auto character : characters_) {
    startPositions_.push_back(
        character->GetComponent<CharacterLib::TransformationCmp>()
            ->GetPosition());
  }
}

void CallbackContainer::CreateWalkPath() {
  wayPath_.clear();
  // A Square
  wayPath_.push_back(VistaVector3D(2.f, 0.f, 0.f));
  wayPath_.push_back(VistaVector3D(2.f, 0.f, 2.f));
  wayPath_.push_back(VistaVector3D(0.f, 0.f, 2.f));
  wayPath_.push_back(VistaVector3D(0.f, 0.f, 0.f));
}
