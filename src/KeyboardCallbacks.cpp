/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "KeyboardCallbacks.h"

#include <memory>

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
KeyboardCallbacks::KeyboardCallbacks(
    int iKey, std::shared_ptr<CallbackContainer> &callbackContainer) {
  key_ = iKey;
  callbackContainer_ = callbackContainer;
}

bool KeyboardCallbacks::Do() {

  switch (key_) {
  case 0:
    callbackContainer_->StartWalking();
    break;
  case 1:
    callbackContainer_->StopWalking();
    break;
  case 2:
    callbackContainer_->SwitchMode();
    break;
  case 3:
    callbackContainer_->NextAnimation();
    break;
  default:
    break;
  }
  return true;
}
