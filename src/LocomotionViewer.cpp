/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "LocomotionViewer.h"

SUPPRESS_WARNINGS_BEGIN

#include <memory>

// Vista
#include <VistaAspects/VistaExplicitCallbackInterface.h>
#include <VistaBase/VistaExceptionBase.h>
#include <VistaKernel/DisplayManager/VistaDisplayManager.h>
#include <VistaKernel/DisplayManager/VistaWindow.h>
#include <VistaKernel/GraphicsManager/VistaGraphicsManager.h>
#include <VistaKernel/InteractionManager/VistaKeyboardSystemControl.h>

SUPPRESS_WARNINGS_END

// CharacterLib
#include <CharacterLib/Components/RenderCmp.hpp>
#include <CharacterLib/Engine.hpp>
#include <CharacterLib/Manager.hpp>
#include <CharacterLib/SmartbodyCharacter.hpp>

#include "KeyboardCallbacks.h"

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS, FUNCTION-PROTOTYPES             */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
LocomotionViewer::~LocomotionViewer() {
  VistaKeyboardSystemControl *keyCtrl =
      GetVistaSystem()->GetKeyboardSystemControl();
  keyCtrl->UnbindAction(' ');
  keyCtrl->UnbindAction('s');
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
bool LocomotionViewer::Init(int argc, char *argv[]) {
  vistaSystem_ = std::make_unique<VistaSystem>();

  std::list<std::string> liSearchPaths;

  // here is a shared configfile used for the demos
  liSearchPaths.push_back("configfiles/");
  liSearchPaths.push_back("../configfiles/");
  vistaSystem_->SetIniSearchPaths(liSearchPaths);

  // ViSTA's standard intro message
  vistaSystem_->IntroMsg();

  if (!vistaSystem_->Init(argc, argv))
    return false;

  if (vistaSystem_->GetDisplayManager()->GetDisplaySystem() == 0)
    VISTA_THROW("No DisplaySystem found", 1);

  // Get the EventManager from the system
  eventManager_ = vistaSystem_->GetEventManager();

  CharacterLib::Engine::GetEngine()->InitEngine();

  ReadConfigurationFile();
  CreateCharacters();
  CreateCallbacks();

  return true;
}

void LocomotionViewer::Run() {
  // Run the VE
  vistaSystem_->Run();
}

void LocomotionViewer::ReadConfigurationFile() {
  configurationParser_ = std::make_unique<ConfigurationParser>();
  animationNames_ = configurationParser_->ReadConfigurationFile(
      "MotionSetup.ini", walkingCycleData_);
}

void LocomotionViewer::CreateCharacters() {
  CharacterLib::Manager *manager = CharacterLib::GetManager();

  characters_.clear();

  characters_.push_back(manager->CreateEntity<CharacterLib::SmartbodyCharacter>(
      "Rachel_0", CharacterLib::SmartbodyCharacter::RACHEL, 1.0f,
      VistaVector3D(-3.f, 0.0f, -8.0f)));
  characters_.push_back(manager->CreateEntity<CharacterLib::SmartbodyCharacter>(
      "Brad_0", CharacterLib::SmartbodyCharacter::BRAD, 1.0f,
      VistaVector3D(0.f, 0.f, -8.0f)));

  for (auto character : characters_) {
    auto locomotionCmp = character->GetComponent<CharacterLib::LocomotionCmp>();
    locomotionCmp->SetWalkingCycle(walkingCycleData_);
    locomotionCmp->SetSteeringType(CharacterLib::LocomotionCmp::CYCLE);

    auto renderCmp = character->GetComponent<CharacterLib::RenderCmp>();
    renderCmp->SetTextOverlayText(walkingCycleData_.animationFile_);
  }
}

void LocomotionViewer::CreateCallbacks() {
  callbackContainer_ = std::make_shared<CallbackContainer>(
      characters_, animationNames_, walkingCycleData_);
  VistaKeyboardSystemControl *keyCtrl =
      GetVistaSystem()->GetKeyboardSystemControl();
  keyCtrl->BindAction(' ', new KeyboardCallbacks(0, callbackContainer_));
  keyCtrl->BindAction('s', new KeyboardCallbacks(1, callbackContainer_));
  keyCtrl->BindAction('r', new KeyboardCallbacks(2, callbackContainer_));
  keyCtrl->BindAction('t', new KeyboardCallbacks(3, callbackContainer_));
}
