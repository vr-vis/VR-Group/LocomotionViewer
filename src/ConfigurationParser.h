/*============================================================================*/
/* This class will read in scenes from xml files and visualize them on a      */
/* SceneGraph.	Simply initialize a SceneReader and use the Read method.      */
/*============================================================================*/

#ifndef SRC_CONFIGURATIONPARSER_H_
#define SRC_CONFIGURATIONPARSER_H_

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

SUPPRESS_WARNINGS_BEGIN

#include <string>

#include <VistaAspects/VistaPropertyAwareable.h>

SUPPRESS_WARNINGS_END

#include <CharacterLib/Components/LocomotionCmp.hpp>
/*============================================================================*/
/* MACROS AND DEFINES                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class ConfigurationParser {
public:
  ConfigurationParser() = default;
  virtual ~ConfigurationParser() = default;

  std::vector<std::string>
  ReadConfigurationFile(std::string fileName,
                        CharacterLib::WalkingCycleData &walkingCycleData);

private:
  void ParseConfiguration(VistaPropertyList propertyList,
                          CharacterLib::WalkingCycleData &walkingCycleData);
  std::vector<std::string> ParseAnimationNames(VistaPropertyList propertyList);

  std::string configurationFile_;
};

/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

#endif // SRC_CONFIGURATIONPARSER_H_
