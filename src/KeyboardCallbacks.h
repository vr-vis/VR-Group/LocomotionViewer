//------------------------------------------------------------------------------
// Project CharacterLoading
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                  License
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------
#ifndef SRC_KEYBOARDCALLBACKS_H_
#define SRC_KEYBOARDCALLBACKS_H_

#include <memory>
#include <vector>

SUPPRESS_WARNINGS_BEGIN
// ViSTA
#include "VistaAspects/VistaExplicitCallbackInterface.h"
#include "VistaBase/VistaVector3D.h"

SUPPRESS_WARNINGS_END

#include "CallbackContainer.h"

#include "CharacterLib/Entity.hpp"

class KeyboardCallbacks : public IVistaExplicitCallbackInterface {
public:
  explicit KeyboardCallbacks(
      int iKey, std::shared_ptr<CallbackContainer> &callbackContainer);
  virtual ~KeyboardCallbacks() = default;

  bool Do() override;

private:
  int key_;
  std::shared_ptr<CallbackContainer> callbackContainer_;
};

#endif // SRC_KEYBOARDCALLBACKS_H_
