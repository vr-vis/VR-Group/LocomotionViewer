//------------------------------------------------------------------------------
// Project CharacterLoading
//
// Copyright (c) 2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualisation Group.
//------------------------------------------------------------------------------
//                                  License
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------
#ifndef SRC_CALLBACKCONTAINER_H_
#define SRC_CALLBACKCONTAINER_H_

SUPPRESS_WARNINGS_BEGIN

#include <string>
#include <vector>

#include <VistaBase/VistaVector3D.h>

SUPPRESS_WARNINGS_END

#include "CharacterLib/Components/LocomotionCmp.hpp"
#include "CharacterLib/Entity.hpp"

class CallbackContainer {
public:
  explicit CallbackContainer(std::vector<CharacterLib::Entity *> characters,
                             std::vector<std::string> animationNames,
                             CharacterLib::WalkingCycleData walkingCycleData);
  virtual ~CallbackContainer() = default;

  void StartWalking();

  void StopWalking();

  void SwitchMode();

  void NextAnimation();

private:
  void SaveStartPositions();
  void CreateWalkPath();

  unsigned int currentAnimation_ = 0;

  bool inSingleAnimationMode = true;

  CharacterLib::WalkingCycleData walkingCycleData_;
  std::vector<CharacterLib::Entity *> characters_;
  std::vector<VistaVector3D> startPositions_;
  std::vector<VistaVector3D> wayPath_;
  std::vector<std::string> animtionNames_;
};

#endif // SRC_CALLBACKCONTAINER_H_
