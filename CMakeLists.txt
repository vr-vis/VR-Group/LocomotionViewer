# $Id$

cmake_minimum_required( VERSION 2.8 )
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

project( LocomotionViewer )

list( APPEND CMAKE_MODULE_PATH "$ENV{VISTA_CMAKE_COMMON}" )
include( VistaCommon )
unix_require_cpp14()

include( cpplint )
include( CTest )
enable_testing()
include( WarningLevels )

vista_set_version( LocomotionViewer HEAD HEAD 1 0 svn_rev )
vista_use_package( VistaCoreLibs REQUIRED VistaKernel FIND_DEPENDENCIES )
vista_use_package( CharacterLib REQUIRED FIND_DEPENDENCIES )

file(GLOB LOCOMOTIONVIEWER_SOURCES src/*.*)
source_group("Source" FILES ${LOCOMOTIONVIEWER_SOURCES})

add_executable( LocomotionViewer ${LOCOMOTIONVIEWER_SOURCES})

target_include_directories( LocomotionViewer
	PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}
	PUBLIC ${CMAKE_CURRENT_BINARY_DIR}
)

target_link_libraries( LocomotionViewer
	${VISTA_USE_PACKAGE_LIBRARIES} # contains all libraries from vista_use_package() calls
)

set_warning_levels_RWTH(LocomotionViewer)
add_test_cpplint(NAME "LocomotionViewer--cpplint"
  ${LOCOMOTIONVIEWER_SOURCES}
)

vista_configure_app( LocomotionViewer )
vista_install( LocomotionViewer )
vista_create_default_info_file( LocomotionViewer )

